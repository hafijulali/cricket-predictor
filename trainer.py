from joblib import dump
import numpy as np
from pandas.io.parsers import read_csv
from sklearn.preprocessing import LabelEncoder
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score

def put_data_label(clean_data):
    venue_encoder, team_encoder = LabelEncoder(), LabelEncoder()
    clean_data['venue'] = venue_encoder.fit_transform(clean_data['venue'])
    clean_data['batting_team'] = team_encoder.fit_transform(
        clean_data['batting_team'])
    clean_data['bowling_team'] = team_encoder.fit_transform(
        clean_data['bowling_team'])
    return venue_encoder, team_encoder

def get_feature_target(clean_data):
    return clean_data.iloc[:,[0,1,2,3]].values, clean_data.iloc[:,4].values


def split_data(independent_variable, target_variable):
    return train_test_split(
        independent_variable, target_variable, test_size=0.25, random_state=0)


def train_model(independent_variable_train_data, independent_variable_test_data, target_variable_train_data, target_variable_test_data):
    scaler = StandardScaler()
    independent_variable_train_data = scaler.fit_transform(independent_variable_train_data)
    independent_variable_test_data = scaler.transform(independent_variable_test_data)
    regressor = RandomForestRegressor(n_estimators=100, max_features=None)
    regressor.fit(independent_variable_train_data, target_variable_train_data)
    return regressor

def print_metrics(model, independent_variable, target_variable):
    predicted_target_variable = model.predict(independent_variable)
    print("Accuracy:",accuracy_score(target_variable, predicted_target_variable))
    print("Precision:",precision_score(target_variable, predicted_target_variable))
    print("Recall:",recall_score(target_variable, predicted_target_variable))
    print("F1 Score:",f1_score(target_variable, predicted_target_variable))



def save_objects(regressor, venue_encoder, team_encoder):
    print("[INFO] Saving Model and Encoders")
    dump(regressor, 'predictor_model.joblib')
    dump(venue_encoder, 'venue_encoder.joblib')
    dump(team_encoder, 'team_encoder.joblib')


if __name__ == "__main__":
    clean_data = read_csv('clean_data.csv')
    encoders = put_data_label(clean_data)
    independent_variable, target_variable = get_feature_target(clean_data)
    train_test_data = split_data(independent_variable, target_variable)
    regressor = train_model(*train_test_data)
    print_metrics(regressor, independent_variable, target_variable)
    save_objects(regressor, *encoders)
